#include <stdio.h>

int main(int argc, char *argv[])
{
  int numbers[4] = {2};
  char name[4] = "a";

  // first print them out raw
  printf("The size of name as char array: %ld\n", sizeof(name));
  printf("numbers %d %d %d %d\n", numbers[0], numbers[1], numbers[2],numbers[3]);
  printf("name %c %c %c %c\n", name[0], name[1], name[2], name[3]);

  // setup the numbers
  numbers[0] = 1;
  numbers[1] = 2;
  numbers[2] = 3;
  numbers[3] = 4;

  // setup the name
  // This does not work with C style string literals.
  name[0] = 'Z';
  name[1] = 'e';
  name[2] = 'd';
  name[3] = '\0';
  
  // print after init
  printf("numbers %d %d %d %d\n", numbers[0], numbers[1], numbers[2],numbers[3]);
  printf("name %c %c %c %c\n", name[0], name[1], name[2], name[3]);

  // print the name like a string
  printf("name: %s\n", name);
  
  // another way to use name
  char *another = "Zed";

  printf("another: %s\n", another);

  printf("The size of another as C string literal: %ld\n", sizeof(another));
  printf("another each: %c %c %c %c\n",
          another[0], another[1],
          another[2], another[3]);

  return 0;
}
