#include <stdio.h>

int main()
{
  printf("Convert 'nul byte' to int: %d.\n", ( int )'\0');
  printf("Convert 'a' to int: %d.\n", ( int )'a');
  printf("Convert 'b' to int: %d.\n", ( int )'b');
  printf("Convert 'c' to int: %d.\n", ( int )'c');

  return 0;
}
