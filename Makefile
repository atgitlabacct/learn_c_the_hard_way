CFLAGS=-Wall -g # -g sets debugging, -Wall turns on all warnings

default: all

clean:
	rm -f ex1 ex3 ex4 ex5
all: clean
	cc ex1.c -o ex1
	cc ex3.c -o ex3
	cc ex4.c -o ex4
	cc ex5.c -o ex5
