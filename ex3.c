#include <stdio.h>

int main()
{
  int age = 10;
  float height = 72.25;

  printf("I am %d years old.\n", age);
  printf("I am %f inches tall.\n", height);
  printf("\n\n");

  printf("Some escape sequences\n");
  printf("audio alert bell \tBEL\t\\a\t%d\n", '\a');
  printf("backspace        \tBS \t\\b\t%d\n", '\b'); 
  printf("double quote     \t\"   \t\\\"\t%d\n", '\"'); 
  printf("\n\n");

  printf("Some format sequences\n");
  printf("%s\n", "Hello Cruel World"); // Normal output
  printf("%25s\n", "Hello Cruel World"); // Fills left side with white space as needed
  printf("%.10s\n", "Hello Cruel World"); // Printf first 10
  printf("%-20s....\n", "Cruel World"); // Print at least 20, if smaller white space at end
  printf("\n\n");

  printf("Nubmer: %d\n", 1234);
  printf("Padded: %08d\n", 1234);
  printf("Integer: %i\n", 1234);
  printf("Float: %3.2f\n", 1234.232);
  printf("Print the percent sign: %%\n");

  return 0;
}
